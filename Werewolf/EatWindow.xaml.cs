﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Werewolf;
using WerewolfCore;

namespace WerewolfWPF
{
    /// <summary>
    /// Interaction logic for EatWindow.xaml
    /// </summary>
    public partial class EatWindow : Window
    {
        Game g = MainWindow.game;

        public EatWindow()
        {
            InitializeComponent();
            EatBox.ItemsSource = g.getVillager();

        }

        private void EatBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (EatBox.SelectedItem != null)
            {
                g.removePerson((Person)EatBox.SelectedItem);

            }
            Close();
        }
    }
}

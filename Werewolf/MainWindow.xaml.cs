﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using WerewolfCore;
using WerewolfWPF;

namespace Werewolf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static Game game = new Game();


        public MainWindow()
        {
            InitializeComponent();
            game.populateVillage();
            PlayersBox.ItemsSource = game.getVillage();

        }


        private void next_phase_Click(object sender, RoutedEventArgs e)
        {
            game.changePhase();
            PhaseBox.Text = game.NightTime ? "Night" : "Day";
        }

        private void Eat_Click(object sender, RoutedEventArgs e)
        {
            if (PhaseBox.Text == "Day")
            {
                Eat.IsEnabled = false;
            }
            else
            {
                Eat.IsEnabled = true;
            }
            EatWindow ew = new EatWindow();
            ew.Show();
        }

        private void Lynch_Click(object sender, RoutedEventArgs e)
        {
            if (PhaseBox.Text == "Day")
            {
                Eat.IsEnabled = true;
            }
            else
            {
                Eat.IsEnabled = false;
            }

            LynchWindow lw = new LynchWindow();
            lw.ShowDialog();
        }

        protected void Players_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ConsoleBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ConsoleBox.Text = $" number of villgers: {game.numberOfvillagers()} and number of werewolfs: {game.numberOfwerewolfs()}" ;
        }

        private void PhaseBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void AddPlayer_Click(object sender, RoutedEventArgs e)
        {
            AddPlayer ap = new AddPlayer();
            ap.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Werewolf;
using WerewolfCore;


namespace WerewolfWPF
{
    /// <summary>
    /// Interaction logic for LynchWindow.xaml
    /// </summary>
    public partial class LynchWindow : Window 
    {
        Game g = MainWindow.game;
        public LynchWindow()
        {
            InitializeComponent();
            LynchBox.ItemsSource = g.getVillage();
            
        }

        private void LynchBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(LynchBox.SelectedItem != null)
            {
                g.removePerson((Person) LynchBox.SelectedItem);
                
                
            }
            Close();
            
            
        }
    }
}

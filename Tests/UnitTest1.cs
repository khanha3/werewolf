using NUnit.Framework;
using WerewolfCore;

namespace Tests
{
    public class Tests
    {
        private Game _game; 
        
        
        [SetUp]
        public void Setup()
        {
            _game = new Game();
            _game.populateVillage();
        }
        
        [Test]
        public void TestGamePhase()
        {

            _game.changePhase();
            
            Assert.AreEqual(false,_game.NightTime);
        }
        [Test]
        public void TestGameVillagerNumber()
        {
           
            var result = _game.numberOfvillagers();

            Assert.AreEqual(9, result);
        }
        [Test]
        public void TestGameWerewolfNumber()
        {
            var result = _game.numberOfwerewolfs();

            Assert.AreEqual(3, result);
        }

        [TearDown]
        public void TearDown()
        {
            _game.flushvillage();
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using WerewolfERDB.GameModels;

namespace WerewolfCore
{
    public class Game
    {
        
        private int numberOfPlayers;
        private List<Person> Village = new List<Person>();

        public bool NightTime { get; private set; }
        

        public Game()
        {
            NightTime = true;

        }

        public void flushvillage()
        {
            Village.Clear();
        }

        
        public void changePhase()
        {
            if (NightTime)
            {
                NightTime = false;
            }
            else 
            {
                NightTime = true;
            }
        }

        public void removePlayer(string name)
        {
            Village.RemoveAll(x => x.Name == name);
        }

        public void populateVillage()
        {
            using (var db = new GameContext())
            {
                foreach(Players p in db.Players) 
                {
                    if (String.Equals(p.Type,"Werewolf"))
                    {
                        Village.Add(new Werewolf(p.Name));
                    }
                    else if (String.Equals(p.Type, "Villager"))
                    {
                        Village.Add(new Villager(p.Name));
                    }
                    
                }
            }

        }

        public void addplayer(string pname , string ptype)
        {
            using (var db = new GameContext())
            {
                Players newPlayer = new Players()
                {
                    Name = pname,
                    Type = ptype,
                    SesstionId = 1

                };

                db.Add(newPlayer);

                db.SaveChanges();

            }
        }

        public void removePerson(Person person)
        {
            using (var db = new GameContext())
            {
                var query =
                    from Name in db.Players
                    select Name;

                query = query.Where(x => x.Type.Equals(person.Name));

                db.Remove(db.Players.Single(a => a.Name.Equals(person.Name)));
                db.SaveChanges();
            }

        }

        public int numberOfvillagers() 
        {
            using (var db = new GameContext())
            {
                var query =
                    from Type in db.Players
                    select Type;

                query = query.Where(x => x.Type.Equals("Villager"));

                return query.ToList().Count;

            }
        }

        public int numberOfwerewolfs()
        {

            using( var db = new GameContext())
            {
                var query =
                    from Type in db.Players
                    select Type;

                query = query.Where(x => x.Type.Equals("Werewolf"));

                return query.ToList().Count;
                
            }

        }

        public List<Person> getVillage() 
        {
            return Village;
        }

        public List<Person> getVillager()
        {
            return Village.FindAll(item => item is Villager);
        }

    }
}

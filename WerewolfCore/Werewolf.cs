﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WerewolfCore
{
    public class Werewolf: Person
    {
        public Werewolf(string name) : base(name)
        {
        }

        public string Eat(Person person)
        {
            return $"{person.Name} eaten";
        }

        // public override string ToString()
        // {
        //     return this;
        // }
    }
}

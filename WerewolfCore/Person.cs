﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WerewolfCore
{
    public abstract class Person
    {
        public string Name { get; set; }

        public  Person(string name)
        {
           Name = name;
        }

        public void lynch()
        {

        }

        public void talk()
        {
            Console.WriteLine("i am talking");
        }

        public string temp()
        {
            return "test";
        }

        public string accuse(Person person)
        {
            return $"i think that {person.Name} is the werewolf";
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}

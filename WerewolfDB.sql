CREATE DATABASE Werewolf;

USE Werewolf;

-- ****************** SqlDBM: Microsoft SQL Server ******************
-- ******************************************************************

-- ************************************** [dbo].[Session]

CREATE TABLE [dbo].[Session]
(
 [SesstionID] int IDENTITY (1, 1) NOT NULL ,

 CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED ([SesstionID] ASC)
);
GO


-- ************************************** [dbo].[Players]

CREATE TABLE [dbo].[Players]
(
 [PlayerID]   int IDENTITY (1000, 1) NOT NULL ,
 [Name]       varchar(50) NOT NULL ,
 [Type]       varchar(50) NOT NULL ,
 [SesstionID] int NOT NULL ,


 CONSTRAINT [PK_Players] PRIMARY KEY CLUSTERED ([PlayerID] ASC),
 CONSTRAINT [FK_18] FOREIGN KEY ([SesstionID])  REFERENCES [dbo].[Session]([SesstionID])
);
GO


CREATE NONCLUSTERED INDEX [fkIdx_18] ON [dbo].[Players]
 (
  [SesstionID] ASC
 )

GO


--inserted vals 1-4 using this statement
SET IDENTITY_INSERT Session ON;
INSERT INTO Session(SesstionID)
VALUES (4);
SET IDENTITY_INSERT Session OFF;


DROP TABLE Players;




--insert players--

INSERT INTO Players(Name, Type, SesstionID)
VALUES ('nish', 'Villager', 1);


INSERT INTO Players(Name, Type , SesstionID)
VALUES ('ross', 'Villager', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('john', 'Villager', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('jess', 'Villager', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('Spider', 'Werewolf', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('hiro', 'Villager', 1);

INSERT INTO Players(Name, Type, SesstionID)
VALUES ('tom', 'Villager', 1);

INSERT INTO Players(Name, Type, SesstionID)
VALUES ('zod', 'Werewolf', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('ironman', 'Villager', 1);

INSERT INTO Players(Name, Type ,SesstionID)
VALUES ('sony', 'Villager' , 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('apple', 'Villager', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('zero', 'Villager', 1);

INSERT INTO Players(Name, Type , SesstionID)
VALUES ('alex', 'Villager' , 1 );


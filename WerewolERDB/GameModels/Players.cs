﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WerewolfERDB.GameModels
{
    public partial class Players
    {
        [Key]
        [Column("PlayerID")]
        public int PlayerId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string Type { get; set; }
        [Column("SesstionID")]
        public int SesstionId { get; set; }

        [ForeignKey(nameof(SesstionId))]
        [InverseProperty(nameof(Session.Players))]
        public virtual Session Sesstion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WerewolfERDB.GameModels
{
    public partial class Session
    {
        public Session()
        {
            Players = new HashSet<Players>();
        }

        [Key]
        [Column("SesstionID")]
        public int SesstionId { get; set; }

        [InverseProperty("Sesstion")]
        public virtual ICollection<Players> Players { get; set; }
    }
}
